;; 在状态栏显示行号
(setq line-number-mode t)

;; 显示左侧行号
(global-linum-mode 1)

;; 设置标题栏
(setq frame-title-format "Emacs")

;; 关闭 Emacs 启动时的画面
(setq inhibit-startup-message t)

;; 设置启动时窗口的长宽
(setq initial-frame-alist '((width . 160) (height . 40)))

;; 让 Emacs 可以直接打开和显示图片
(setq auto-image-file-mode t)

;; 不让光标闪动
(blink-cursor-mode 0)

;; 添加软件源
(require 'package)

(add-to-list 'package-archives '("melpa-cn" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/") t)
(add-to-list 'package-archives '("org-cn"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")   t)
(add-to-list 'package-archives '("gnu-cn"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")   t)

;; 自己安装的软件包
;; (add-to-list 'package-selected-packages '(evil) t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(cnfonts magit evil)))

(package-initialize)

;; 设置外观
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; 开启 Evil mode
(require 'evil)
(evil-mode 1)

;; 使用 cnfonts
(require 'cnfonts)
;; 让 cnfonts 在 Emacs 启动时自动生效
(cnfonts-mode 1)
;; 添加两个字号增大缩小的快捷键
(define-key cnfonts-mode-map (kbd "C--") #'cnfonts-decrease-fontsize)
(define-key cnfonts-mode-map (kbd "C-=") #'cnfonts-increase-fontsize)

;; 文件自动备份设置
(setq
 backup-by-copying t               ; 自动备份
 backup-directory-alist   
  '(("." . "~/.emacs.d/backups"))  ; 自动备份在此目录下
 delete-old-versions t             ; 自动删除旧的备份文件
 kept-new-versions   3             ; 保留最近的3个备份文件
 kept-old-versions   1             ; 保留最早的1个备份文件
 version-control     t)            ; 多次备份
